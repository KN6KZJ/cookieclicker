A simple web automation app that plays the "Cookie Clicker" web game.

The software auto clicks the middle cookie as fast as possible to gain initial cookies that are then later spent on upgrades.

The app then constantly attempts to purchase upgrades based on the cookies per minute rate.

I made this software as an excerise in web automation. This wasn't meant to be used as cheating software although the game is not multiplayer so you're not cheating against anyone.